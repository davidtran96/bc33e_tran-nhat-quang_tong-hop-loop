var numArr = [];
function getNumber() {
  var n = document.getElementById("txt-input").value * 1;
  numArr.push(n);
  document.getElementById("showArr").innerHTML = numArr;
}
function soNguyenTo(x) {
  if (x < 2) {
    return false;
  }
  for (var i = 2; i <= Math.sqrt(x); i++) {
    if (x % i == 0) {
      return false;
    }
  }
  return true;
}

function inSoNguyenTo() {
  var a = [];
  var contentHtml = "";
  for (var i = 0; i < numArr.length; i++) {
    if (soNguyenTo(numArr[i])) {
      a = numArr[i];
      contentHtml += a + " ";
    }
  }
  document.getElementById("result").innerHTML = contentHtml;
}
